# Overview

This project is a prototype of website that lets users keep track of their BTC addresses like CoinTracker. The features implemented in this app are not exhaustive, however, the app in its current state has everything required to quickly add more features as desired.

> I was unfortunately blocked by Cloudflare early on in development and
> was unable to make any API calls to either Blockchair or
> Blockchain.com. However, I decided to continue developing with mock
> data.

The app consists of a few different components described below.

## Database
A TinyDB database was used for the purpose of this project.

**Limitations**

 1. It is schemaless and provides basic persistence using a JSON file, however it lacks advanced querying features.

## REST Interface
General purpose API for managing addresses; only endpoints for adding addresses and managing transactions was implemented. Requests are to this API are validated.

**Limitations**

 1. Only adding addresses and managing transactions is currently
   supported.  
   
 2. I skipped adding functionality for deleting addresses to
   save time.
   
 3. I skipped extensive unit tests to save time
 4. I skipped any data aggregation to show a "summary" for transactions; total etc. This was mainly due to the fact that I got rate limited and ended up wasting some time trying to figure out how to bypass.

## Background Worker With Real-time Communication
A background worker was added to sync transactions in the background. The worker exposes a queue that can be loaded with BTC addresses. Each address dequeued one by one and all transactions for it are queried from a downstream API and saved to the database.

A SocketIO event is emited when all the transactions are loaded

**Limitations**

 1. Does not use real data because I was rate limited. An artificial delay was added to simulate work that takes time.
 2. Does not have any fault tolerance or error handling features; e.g to work arounds for rate limits, downstream APIs that don't respond etc.
 3.  I skipped extensive unit tests to save time

## Frontend
A React application for adding addresses and viewing transactions. The application also gets updates in real time and displays them as notifications.

**Limitations**

 1. It only has enough features to demo interactions with the backend
 2. No testing to save time
 3. You will likely see notifications appear twice; this because the app is running in StrictMode in development.
 

# Running The App
Clone the repo and follow these steps. I use Python 3.8.8 and Node v16.3.0.

## Backend

    cd backend
    python -m venv .venv
    .\.venv\Scripts\activate 
    pip install -r requirements.txt
    python -m unittest
    python .\run.py

## Frontend:

    cd frontend
    yarn install
    yarn dev

1. Click on Add Address to add a new Address. The address will appear on the screen with the status "Syncing"
2. After a few seconds a notification will appear and the Status will automatically change to "Synced"
3. The address can be clicked to view all transactions associated with it.
4. If the same address is added again, an error notification will appear.

# Recommended Improvements

 - Move Background Task to its own project, maybe to GCP using Cloud Functions and Cloud Tasks
 - Use a proper database like PostgresSQL; this will likely require adding more specific models in the backend e.g for DTOs data transfer, persistence models for ORM etc 
 - Introduce User Authentication
 - Productionize code; run in docker containers and support deployment to multiple environments
 - Add more unit tests
 - Add API tests for the backend
