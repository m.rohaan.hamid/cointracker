import { useEffect } from "react";
import { socket } from "./api/socket";
import { useRoutes } from "react-router-dom";
import IndexRoute from "./routes/IndexRoute";
import AddressRoute from "./routes/AddressRoute";
import { useQueryClient } from "react-query";
import { notifications } from "@mantine/notifications";

function App() {
  const queryClient = useQueryClient();

  useEffect(() => {
    socket.on("tx_sync", (data) => {
      notifications.show({
        title: `Transactions for address: ${data.address} were synced`,
        message: "UI will be refreshed.",
      });
      queryClient.invalidateQueries();
    });
  }, []);

  const routes = useRoutes([
    {
      path: "/",
      element: <IndexRoute />,
    },
    {
      path: "/address/:address",
      element: <AddressRoute />,
    },
  ]);
  return routes;
}

export default App;
