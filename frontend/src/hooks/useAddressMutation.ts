import { useMutation } from "react-query"
import { addAddress } from "../api/api"

export const useAddressMutation = (onSuccess: () => void, onError: (reason: string) => void) => {
    return useMutation(addAddress, {
        onSuccess: onSuccess,
        onError
    })
}