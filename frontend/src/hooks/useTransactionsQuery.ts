import { useQuery } from "react-query";
import { getTransactions } from "../api/api";

export const useTransactionsQuery = (address?: string) => {
  return useQuery(["transactions", address], () => getTransactions(address), {
    enabled: !!address,
  });
};
