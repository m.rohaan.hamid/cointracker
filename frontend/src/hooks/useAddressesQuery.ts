import { useQuery } from "react-query"
import { getAddresses } from "../api/api"

export const useAddressesQuery = () => {
    return useQuery("addresses", getAddresses)
}