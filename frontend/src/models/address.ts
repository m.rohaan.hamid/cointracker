// Keeping snake_case to save time. TS prefers camelcase.
export interface Address {
    sync_status: string;
    symbol: string;
    address: string;
}