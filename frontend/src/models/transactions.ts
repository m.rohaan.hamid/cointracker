// Keeping snake_case to save time. TS prefers camelcase.
export interface TransactionIn{
    in_address: string;
    in_value: number
}

export interface TransactionOut {
    out_address: string;
    out_value: number;
}

export interface Transaction {
    hash: string;
    tx_in: TransactionIn[];
    tx_out: TransactionOut[];
}