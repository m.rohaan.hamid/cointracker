import { FC, useState } from "react";

import {
  Center,
  Container,
  Button,
  List,
  Group,
  ThemeIcon,
  Badge,
  Text,
  Box,
  Modal,
  TextInput,
} from "@mantine/core";
import { IconCoinBitcoin } from "@tabler/icons-react";
import { useAddressesQuery } from "../hooks/useAddressesQuery";
import { NavLink } from "react-router-dom";
import { useDisclosure } from "@mantine/hooks";
import { useAddressMutation } from "../hooks/useAddressMutation";
import { notifications } from "@mantine/notifications";
import { useQueryClient } from "react-query";

interface IAddAddressModalProps {
  opened: boolean;
  onClose: () => void;
  onAddressAdded: (address: string) => void;
}

const AddAddressModal: FC<IAddAddressModalProps> = (props) => {
  const [value, setValue] = useState("");

  const handleAddressAdd = () => {
    props.onAddressAdded(value);
    props.onClose();
  };

  return (
    <Modal
      opened={props.opened}
      onClose={props.onClose}
      title={<Text fw="bold">Add new address</Text>}
    >
      <Group>
        <TextInput
          w="100%"
          value={value}
          onChange={(event) => setValue(event.currentTarget.value)}
        />
        <Button
          color="teal"
          variant="light"
          radius="lg"
          onClick={handleAddressAdd}
        >
          Add
        </Button>
      </Group>
    </Modal>
  );
};

const IndexRoute: FC = () => {
  const {data} = useAddressesQuery();
  const queryClient = useQueryClient();


  const handleMutationError = (reason: string) => {
    notifications.show({
      title: "Error occured while trying to add address",
      message: reason,
    });
  };

  const handleMutationSuccess = () => {
    queryClient.invalidateQueries();
  };

  const addressMutation = useAddressMutation(handleMutationSuccess, handleMutationError);
  const [opened, { open, close }] = useDisclosure(false);

  const handleAddressAdded = (address: string) => {
    addressMutation.mutate(address);
  };

  return (
    <Container>
      <AddAddressModal
        opened={opened}
        onClose={close}
        onAddressAdded={handleAddressAdded}
      />
      <Container>
        <Center mb="lg">
          <Text size="xl" fw="bold" mr="md">
            My Addresses
          </Text>
          <Button
            size="xs"
            fw="bold"
            variant="light"
            color="teal"
            radius="lg"
            onClick={open}
          >
            Add New
          </Button>
        </Center>
        <Box>
          {data &&
            data.map((x, i) => (
              <Box
                sx={(theme) => ({
                  background: theme.colors.teal[0],
                  padding: theme.spacing.lg,
                  borderRadius: theme.radius.lg,
                  marginBottom: theme.spacing.md,
                  width: "100%",
                })}
                key={i}
              >
                <NavLink
                  to={"/address/" + x.address}
                  style={{ textDecoration: "none" }}
                >
                  <Group>
                    <ThemeIcon color="teal" size={24} radius="xl">
                      <IconCoinBitcoin size="1rem" />
                    </ThemeIcon>
                    <Text color="dark">{x.address}</Text>
                    <Badge size="xs" color="violet">
                      {x.sync_status}
                    </Badge>
                  </Group>
                </NavLink>
              </Box>
            ))}
        </Box>
      </Container>
    </Container>
  );
};

export default IndexRoute;
