import { FC } from "react";

import {
  Center,
  Container,
  UnstyledButton,
  Group,
  Text,
  Box,
  Collapse,
  Grid,
  ThemeIcon,
} from "@mantine/core";
import { useParams } from "react-router-dom";
import { useTransactionsQuery } from "../hooks/useTransactionsQuery";
import { Transaction } from "../models/transactions";
import { useDisclosure } from "@mantine/hooks";
import { IconChevronDown } from "@tabler/icons-react";

interface ITransactionProps {
  transaction: Transaction;
}

const TransactionCollapse: FC<ITransactionProps> = (props) => {
  const [opened, { toggle }] = useDisclosure(false);

  return (
    <Box w="100%">
      <Group mb={5} w="100%">
        <UnstyledButton
          w="100%"
          sx={(theme) => ({
            background: theme.colors.teal[0],
            padding: theme.spacing.lg,
            borderRadius: theme.radius.lg,
          })}
          onClick={toggle}
        >
          <Group position="apart">
            {props.transaction.hash}
            <ThemeIcon variant="subtle">
              <IconChevronDown />
            </ThemeIcon>
          </Group>
        </UnstyledButton>
      </Group>

      <Collapse in={opened} w="100%">
        <Grid px="xl" py="md">
          <Grid.Col span={6}>
            <Text fw="bold">From</Text>
            {props.transaction.tx_in.map((x) => (
              <div>{x.in_address + ":" + x.in_value/100000000} BTC</div>
            ))}
          </Grid.Col>
          <Grid.Col span={6}>
            <Text fw="bold">To</Text>
            {props.transaction.tx_out.map((x) => (
              <div>{x.out_address + ":" + x.out_value/100000000} BTC</div>
            ))}
          </Grid.Col>
        </Grid>
      </Collapse>
    </Box>
  );
};

const AddressRoute: FC = () => {
  const { address } = useParams();
  const transactionsQuery = useTransactionsQuery(address);

  return (
    <Container>
      <Center mb="lg">
        <Text size="xl" fw="bold" mr="md">
          Transactions for {address}
        </Text>
      </Center>
      {transactionsQuery.data &&
        transactionsQuery.data.map((x, i) => (
          <TransactionCollapse transaction={x} key={i} />
        ))}
    </Container>
  );
};

export default AddressRoute;
