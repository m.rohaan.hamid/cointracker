import { Address } from "../models/address";
import { Transaction } from "../models/transactions";

const baseAddress = "http://127.0.0.1:5000/api"

export const getAddresses = async () => {
    const response = await fetch(`${baseAddress}/addresses`);

    return response.json() as unknown as Address[];
}

export const addAddress = async (address?: string) => {
    const response = await fetch(`${baseAddress}/addresses`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
          },
        body: JSON.stringify({
            address: address,
            symbol: "BTC"
        })
    });

    if(response.status === 200 || response.status === 201)
    {
        return {};
    }
    
    throw await response.text();
}

export const getTransactions = async (address?: string) => {
    if(!address)
    {
        return undefined;
    }

    const response = await fetch(`${baseAddress}/addresses/${address}/transactions`);

    return response.json() as unknown as Transaction[];
}