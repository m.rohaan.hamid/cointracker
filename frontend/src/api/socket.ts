import { io } from 'socket.io-client';

const baseAddress = "http://127.0.0.1:5000"

export const socket = io(baseAddress);