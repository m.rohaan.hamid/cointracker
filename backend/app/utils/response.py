from typing import Any
from flask import jsonify


def problem_response(error: str, code: int = 400):
    return jsonify({'error': error}), code

def success_response(body: Any, code: int = 200):
    return jsonify(body), code

def empty_success_response(code: int = 200):
    return '', code