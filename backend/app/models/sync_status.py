from typing import Literal


SyncStatus = Literal['synced', 'syncing', 'failed', 'unknown']