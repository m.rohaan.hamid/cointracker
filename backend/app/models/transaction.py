from typing import TypedDict, List       

class TransactionIn(TypedDict):
    in_address: str
    in_value: int

class TransactionOut(TypedDict):
    out_address: str
    out_value: int

class Transaction(TypedDict):
    hash: str
    tx_in: List[TransactionIn]
    tx_out: List[TransactionOut]