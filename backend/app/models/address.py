from typing import TypedDict

from app.models.sync_status import SyncStatus


class Address(TypedDict):
    address: str
    sync_status: SyncStatus
    symbol: str