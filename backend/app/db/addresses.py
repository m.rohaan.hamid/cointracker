from typing import List, Literal
from tinydb import Query
from tinydb.operations import delete, set
from app.models.address import Address
from app.models.sync_status import SyncStatus
from app.models.transaction import Transaction
from app.db import tiny_db


addresses_table = tiny_db.table('addresses')
address_query = Query()

def check_address_exists(address: str):
    return addresses_table.contains(address_query.address == address)

def add_address(address: str, symbol: str):
    document: Address = {
        'address': address,
        'symbol': symbol,
        'sync_status': "syncing"
    }

    addresses_table.insert(document)

def set_sync_status(address: str, status: SyncStatus):
    document = {
        'sync_status': status,
    }

    addresses_table.update(document, address_query.address == address)

def delete_transactions(address: str):
    addresses_table.update(delete('transactions'), address_query.address == address)

def add_transactions(address: str, transactions: List[Transaction]):
    addresses_table.update(set('transactions', transactions), address_query.address == address)

def get_addresses()->List[Address]:
    addresses = addresses_table.all()

    # Simulate only returning meta data for each address. Throw away transactions
    return [*map(lambda a: Address({
        'address': a["address"],
        'sync_status': a["sync_status"],
        'symbol': a["symbol"],
    }), addresses)]

def get_transactions(address: str)->List[Transaction]:
    # Simulate only returning meta data for each address
    addresses = addresses_table.get(address_query.address == address)

    return addresses['transactions']