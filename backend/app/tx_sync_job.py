import queue
from threading import Thread
import time
import logging
from typing import List
from app.clients.blockchain_mock import blockchain_client
from app.db import addresses as addresses_table

class TxSyncJob:
    def __init__(self):
        self.logger = logging.getLogger("TxSynJob")
        self.thread = Thread(target=self._tx_sync_worker, daemon=True)
        self.sync_queue = queue.Queue()

    # This is incomplete: I got rate limited both on Blockchain.info and Blockchair.com
    def _get_all_transactions(address: str):
        start_offset = 0
        total_transactions_queried = 0
        query_size = 50

        while True:
            num_transactions, transactions = blockchain_client.get_address_transactions(address, start_offset, query_size)
            total_transactions_queried += len(transactions)

            yield transactions

            if total_transactions_queried >= num_transactions:
                break

            start_offset += query_size 

    def _tx_sync_worker(self):
        # This late import is required so that socket io is completely monkey patched
        # to enable emitting events from a different thread.

        # Maybe there is a better way of doing this but I didn't have time to investigate more.
        from app.flask_app import socket_io

        while True:
            address = self.sync_queue.get()
            self.logger.info("Started processing address: %s", address)

            addresses_table.set_sync_status(address, "syncing")

            try:
                transactions = blockchain_client.get_address_transactions(address)

                addresses_table.add_transactions(address, transactions)
                addresses_table.set_sync_status(address, "synced")

                socket_io.emit("tx_sync", {
                    'address': address
                })
            except Exception as e:
                self.logger.exception(e)
                addresses_table.set_sync_status(address, "failed")

            self.sync_queue.task_done()

            self.logger.info("Finished processing address: %s", address)
    
    def start(self):
        self.thread.start()

tx_sync_job = TxSyncJob()