from time import sleep
from typing import List
import requests

from app.models.transaction import Transaction


class BlockchainClient:
    def __init__(self):
        self.base_url = 'https://mockchain.info'

    def check_address_exists(self, address: str) -> bool:
        if address == "address_that_does_not_exst":
            return False
        return True

    def get_address_transactions(self, address: str, offset: int = 0, limit: int = 50):
        sleep(5)

        mock_transactions: List[Transaction] = [
            {
                "hash": "some-hash-1",
                "tx_in": [
                    {
                        "in_address": "mock_in_address",
                        "in_value": 500
                    }
                ],
                "tx_out": [
                    {
                        "out_address": "mock_out_address",
                        "out_value": 100
                    },
                    {
                        "out_address": "mock_out_address",
                        "out_value": 100
                    },
                    {
                        "out_address": "mock_out_address",
                        "out_value": 100
                    },
                    {
                        "out_address": "mock_out_address",
                        "out_value": 100
                    },
                    {
                        "out_address": "mock_out_address",
                        "out_value": 100
                    }
                ],
            }
        ]

        return mock_transactions

blockchain_client = BlockchainClient()