from typing import List
import requests

from app.models.transaction import Transaction


class BlockchainClient:
    def __init__(self):
        self.base_url = 'https://blockchain.info'

    def _simplify_transactions_response(self, transactions):
        simplified_transactions: List[Transaction] = []

        for transaction in transactions:
            simplified_transaction: Transaction = {}

            simplified_transaction["hash"] = transaction["hash"]
            simplified_transaction["tx_in"] = []
            simplified_transaction["tx_out"] = []

            for input in transaction["inputs"]:
                in_address = input["prev_out"]["addr"]
                in_value = input["prev_out"]["value"]

                simplified_transaction["tx_in"].append({
                    "in_address": in_address,
                    "in_value": in_value
                })

            for out in transaction["out"]:
                out_address = out["addr"]
                out_value = out["value"]

                simplified_transaction["tx_out"].append({
                    "out_address": out_address,
                    "out_value": out_value
                })
        
            simplified_transactions.append(simplified_transaction)

        return simplified_transactions

    def check_address_exists(self, address: str) -> bool:
        endpoint = f'{self.base_url}/rawaddr/{address}'
        response = requests.head(endpoint)
        if response.status_code == 200:
            return True
        elif response.status_code == 404:
            return False
        else:
            return None

    def get_address_transactions(self, address: str, offset: int = 0, limit: int = 50):
        endpoint = f'{self.base_url}/rawaddr/{address}?limit={limit}&offset={offset}'
        response = requests.get(endpoint)
        response.raise_for_status()
        if response.status_code == 200:
            num_transactions = response.json()['n_tx']
            transactions = response.json()['txs']
            return num_transactions, self._simplify_transactions_response(transactions)

        
blockchain_client = BlockchainClient()