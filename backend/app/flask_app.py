from flask_socketio import SocketIO
from flask import Flask, request, jsonify
from flask_cors import CORS
from jsonschema import validate
from tinydb import TinyDB, Query, where
from app.utils.response import problem_response, success_response, empty_success_response
from app.clients.blockchain_mock import blockchain_client
from app.db import addresses as address_table
from app.tx_sync_job import tx_sync_job
import eventlet
eventlet.monkey_patch(os=True,
                     select=True,
                     socket=False,
                     thread=False,
                     time=True)

app = Flask(__name__)
CORS(app)

socket_io = SocketIO(app, async_mode="threading", cors_allowed_origins="*")

post_address_body_schema = {
    "type": "object",
    "properties": {
        "address": {"type": "string"},
        "symbol": {"type": "string", "enum": ["BTC"]}
    },
    "required": ["address", "symbol"],
    "additionalProperties": False
}

@app.route('/api/addresses', methods=['POST'])
def add_address():
    request_body = request.get_json()

    try:
        validate(request_body, post_address_body_schema)
    except Exception:
        return problem_response(f"Invalid request body")
    
    address = request_body['address']
    symbol = request_body['symbol']

    if address_table.check_address_exists(address):
        return problem_response("Address already exists", 409)

    if blockchain_client.check_address_exists(address):
        address_table.add_address(address, symbol)
        tx_sync_job.sync_queue.put(address)
        return empty_success_response(201)
    else:
        return problem_response("Invalid blockchain address", 400)
    
@app.route('/api/addresses', methods=['GET'])
def get_addresses():
    addresses = address_table.get_addresses()
    return success_response(addresses)

@app.route('/api/addresses/<address>/transactions', methods=['GET'])
def get_transactions(address: str):
    transactions = address_table.get_transactions(address)
    return success_response(transactions)
    
@app.route('/api/addresses/<address>/sync-transactions', methods=['PUT'])
def sync_address_transactions(address: str):
    if not address_table.check_address_exists(address):
        return problem_response("Could not find address", 400)

    tx_sync_job.sync_queue.put(address)

    return empty_success_response()
