import unittest
from unittest.mock import MagicMock, patch

from app.models.transaction import Transaction
from app.clients.blockchain import BlockchainClient


class TestBlockchainClient(unittest.TestCase):

    def setUp(self):
        self.client = BlockchainClient()

    def test_simplify_transactions_response(self):
        # Prepare test data
        transactions = [
            {
                "hash": "transaction_hash_1",
                "inputs": [
                    {
                        "prev_out": {
                            "addr": "input_address_1",
                            "value": 10
                        }
                    }
                ],
                "out": [
                    {
                        "addr": "output_address_1",
                        "value": 20
                    }
                ]
            },
            {
                "hash": "transaction_hash_2",
                "inputs": [
                    {
                        "prev_out": {
                            "addr": "input_address_2",
                            "value": 30
                        }
                    }
                ],
                "out": [
                    {
                        "addr": "output_address_2",
                        "value": 40
                    }
                ]
            }
        ]

        # Call the method under test
        simplified_transactions = self.client._simplify_transactions_response(transactions)

        # Assert the results
        expected_transactions = [
            {
                "hash": "transaction_hash_1",
                "tx_in": [
                    {
                        "in_address": "input_address_1",
                        "in_value": 10
                    }
                ],
                "tx_out": [
                    {
                        "out_address": "output_address_1",
                        "out_value": 20
                    }
                ]
            },
            {
                "hash": "transaction_hash_2",
                "tx_in": [
                    {
                        "in_address": "input_address_2",
                        "in_value": 30
                    }
                ],
                "tx_out": [
                    {
                        "out_address": "output_address_2",
                        "out_value": 40
                    }
                ]
            }
        ]

        self.assertEqual(simplified_transactions, expected_transactions)

    @patch('app.clients.blockchain.requests')
    def test_check_address_exists_returns_true(self, mock_requests):
        # Prepare the mock response
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_requests.head.return_value = mock_response

        # Call the method under test
        result = self.client.check_address_exists('address_1')

        # Assert the result
        self.assertTrue(result)

    @patch('app.clients.blockchain.requests')
    def test_check_address_exists_returns_false(self, mock_requests):
        # Prepare the mock response
        mock_response = MagicMock()
        mock_response.status_code = 404
        mock_requests.head.return_value = mock_response

        # Call the method under test
        result = self.client.check_address_exists('address_2')

        # Assert the result
        self.assertFalse(result)

    @patch('app.clients.blockchain.requests')
    def test_check_address_exists_returns_none(self, mock_requests):
        # Prepare the mock response
        mock_response = MagicMock()
        mock_response.status_code = 500
        mock_requests.head.return_value = mock_response

        # Call the method under test
        result = self.client.check_address_exists('address_3')

        # Assert the result
        self.assertIsNone(result)

    @patch('app.clients.blockchain.requests')
    def test_get_address_transactions(self, mock_requests):
        # Prepare the mock response
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.json.return_value = {
            'n_tx': 2,
            'txs': [
                {
                    "hash": "transaction_hash_1",
                    "inputs": [
                        {
                            "prev_out": {
                                "addr": "input_address_1",
                                "value": 10
                            }
                        }
                    ],
                    "out": [
                        {
                            "addr": "output_address_1",
                            "value": 20
                        }
                    ]
                },
                {
                    "hash": "transaction_hash_2",
                    "inputs": [
                        {
                            "prev_out": {
                                "addr": "input_address_2",
                                "value": 30
                            }
                        }
                    ],
                    "out": [
                        {
                            "addr": "output_address_2",
                            "value": 40
                        }
                    ]
                }
            ]
        }
        mock_requests.get.return_value = mock_response

        # Call the method under test
        num_transactions, simplified_transactions = self.client.get_address_transactions('address_1', 0, 50)

        # Assert the results
        expected_num_transactions = 2
        expected_simplified_transactions = [
            {
                "hash": "transaction_hash_1",
                "tx_in": [
                    {
                        "in_address": "input_address_1",
                        "in_value": 10
                    }
                ],
                "tx_out": [
                    {
                        "out_address": "output_address_1",
                        "out_value": 20
                    }
                ]
            },
            {
                "hash": "transaction_hash_2",
                "tx_in": [
                    {
                        "in_address": "input_address_2",
                        "in_value": 30
                    }
                ],
                "tx_out": [
                    {
                        "out_address": "output_address_2",
                        "out_value": 40
                    }
                ]
            }
        ]

        self.assertEqual(num_transactions, expected_num_transactions)
        self.assertEqual(simplified_transactions, expected_simplified_transactions)


if __name__ == '__main__':
    unittest.main()