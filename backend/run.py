from app.flask_app import app, socket_io
from app.tx_sync_job import tx_sync_job
import logging

logging.basicConfig(level=logging.DEBUG)

if __name__ == '__main__':
    tx_sync_job.start()
    socket_io.run(app, debug=True)